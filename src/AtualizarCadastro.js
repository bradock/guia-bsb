import React, {Component} from 'react'
import {Text,View,StyleSheet,Image,Dimensions,ScrollView,TextInput,TouchableHighlight,FlatList,BackHandler} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class AtualizarCadastro extends Component{

    state = {
        load:       "",
        user:       "",
        nome:       "",
        sobrenome:  "",
        data:       "",
        cep:        "",
        email:      "",
        senha:      ""

    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this._pegarDados()
    }
    
    componentWillUnmount() {
        this.backHandler.remove()
    }
    
    handleBackPress = () => {
        this.props.navigation.navigate("Perfil"); // works best when the goBack is async
        return true;
    }

    // pegar dados do usuario

    _pegarDados = async () =>{
        try{
            const user = this.props.navigation.getParam("user");
            //const user = "1";
            return fetch('http://192.168.0.17:3000/usuario_selecionar'+user)
           .then( response =>  response.json()
            ).then( (responseJson) =>{
                //console.log(responseJson[0]);
                this.setState({
                    user:       responseJson[0].pk_usr_usuario,
                    nome:       responseJson[0].nme_usuario,
                    sobrenome:  responseJson[0].sno_usuario,
                    data:       responseJson[0].dta_nascimento != null ? responseJson[0].dta_nascimento.slice(0,10) : null,
                    cep:        responseJson[0].cep_usuario != null ?  responseJson[0].cep_usuario: null ,
                    email:      responseJson[0].eml_usuario,
                    senha:      responseJson[0].pwd_senha,
                })
            })
        }catch(exception){
            console.log(exception);
            this.props.navigation.navigate("NetFalha",{erro: "exception erro network"});
        }
    }

    //componentDidMount(){ // apena uma vez
    //    return fetch('http://192.168.0.17:3000/'+this.state.id) //IPV4
    //    .then((response) => response.json())
    //   .then((responseJson) => {
    //   console.log(responseJson.name);
    // this.setState({
    //   data: responseJson.name
    //})
    //})
    //.catch((error) =>{
    //  console.error(error);
    //  });
    //}

    _postEdit = async () =>{
        try{
            this.setState({load: "Carregando..."});
            await fetch("http://192.168.0.17:3000/edit_usuario",{
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    user: this.state.user,
                    nome: this.state.nome,
                    sobrenome: this.state.sobrenome,
                    data: this.state.data,
                    cep: this.state.cep,
                    email: this.state.email,
                    senha: this.state.senha
                }),
            }).then( (resonse) =>{
                resonse.json()
            }).then( (responseJson) =>{
                console.log("Cadastrado");
                this.setState({load: ""});
                this.props.navigation.navigate("Perfil");
            })

        }catch(execption){
            console.log(execption);
            this.setState({load: ""});
            this.props.navigation.navigate("NetFalha",{erro: "exception erro network"});
        }
    }

    render(){
        return(
            <ScrollView style={estilo.container}>
                <Image style={estilo.imagemSuperior} source={require('../assets/imagemAtualizarCadastro.png')}/>
                <Image style={estilo.iconPerson} source={require("../assets/person.png")}/>
                <Icon name="arrow-left" size={30} style={{marginLeft: 20, marginBottom: 10}} onPress={ () => this.props.navigation.navigate("Perfil")}/>
                <View style={estilo.espacoBranco}>
                    <TextInput value={this.state.user} onChangeText={(text) => this.setState({user: text})}  placeholder="User" placeholderTextColor="black" inlineImageLeft="icon_cadastro" inlineImagePadding={20} style={estilo.inputs}></TextInput>
                    <TextInput value={this.state.nome} onChangeText={(text) => this.setState({nome: text})} placeholder="Nome" placeholderTextColor="black" inlineImageLeft="icon_cadastro" inlineImagePadding={20} style={estilo.inputs}></TextInput>
                    <TextInput value={this.state.sobrenome} onChangeText={(text) => this.setState({sobrenome: text})} placeholder="Sobrenome" placeholderTextColor="black" inlineImageLeft="icon_cadastro" inlineImagePadding={20} style={estilo.inputs}></TextInput>
                    <TextInput value={this.state.data} onChangeText={(text) => this.setState({data: text})} placeholder="Ex: 1998-03-28"  placeholderTextColor="red" inlineImageLeft="icon_data" inlineImagePadding={20} style={estilo.inputs}></TextInput>
                    <TextInput value={this.state.cep} onChangeText={(text) => this.setState({cep: text})} placeholder="CEP" placeholderTextColor="red" inlineImageLeft="icon_home" inlineImagePadding={20} style={estilo.inputs}></TextInput>
                    <TextInput value={this.state.email} onChangeText={(text) => this.setState({email: text})} placeholder="Email" placeholderTextColor="black" inlineImageLeft="icon_email" inlineImagePadding={20} style={estilo.inputs}></TextInput>
                    <TextInput value={this.state.senha} onChangeText={(text) => this.setState({senha: text})} placeholder="Senha" secureTextEntry={true} textContentType="password" placeholderTextColor="black" inlineImageLeft="senha" inlineImagePadding={20} style={estilo.inputs}></TextInput>
                    <Text style={{fontSize: 20, textAlign: "center"}}>{this.state.load}</Text>
                    <TouchableHighlight onPress={this._postEdit} style={estilo.btnAtualizar}>
                        <Text style={{fontSize:30,color: "#fff",textAlign: "center"}}>Atualizar</Text>
                    </TouchableHighlight>
                </View>
            </ScrollView>
        )
    }
}


const estilo = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
        width: '100%'
    },
    imagemSuperior:{
        width: Dimensions.get("window").width,
        height: 259
    },
    iconPerson: {
        width: 140,
        height: 140,
        marginTop: -90,
        marginLeft: Dimensions.get("window").width /3
    },
    espacoBranco:{
       alignItems: 'center'
  
    },
    inputs:{
        borderBottomWidth: 2,
        borderBottomColor: 'gray',
        width: Dimensions.get("window").width - 70,
        fontSize: 16
    },
    btnAtualizar:{
        backgroundColor: "#0998E7",
        width: 150,
        height: 45,
        marginTop: 20,
        marginBottom:20
    }
 
});