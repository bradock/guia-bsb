import React, {Component} from 'react'
import {Text,View,Image,StyleSheet,Dimensions,TextInput,TouchableHighlight,KeyboardAvoidingView,ScrollView,BackHandler} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class CadastroInicial extends Component{

    state = {
        user:       "",
        nome:       "",
        sobrenome:  "",
        email:      "",
        senha:      "",
        load:       ""
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }
    
    componentWillUnmount() {
        this.backHandler.remove()
    }
    
    handleBackPress = () => {
        this.props.navigation.navigate("Login"); // works best when the goBack is async
        return true;
    }

    _postData = async () =>{ 
        try{
            this.setState({load: "Carregando..."});
            await fetch('http://192.168.0.17:3000/addUsuario', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    user :     this.state.user,
                    nome:      this.state.nome,
                    sobrenome: this.state.sobrenome,
                    email:     this.state.email,
                    senha :    this.state.senha
                }),
            }).then( (response) => {
                response.json()
            }).then( (responseJson) =>{
                this.setState({
                    user:       "",
                    nome:       "",
                    sobrenome:  "",
                    email:      "",
                    senha:      "",
                })
                console.log("CADASTRADO");
                this.setState({load: ""});
                this.props.navigation.navigate("ResultadoCadastro");
            })
        }catch(exception){
            console.log(exception);
            this.setState({
                user:       "",
                nome:       "",
                sobrenome:  "",
                data:       "",
                cep:        "",
                email:      "",
                senha:      "",
                load:       ""
            })
            this.props.navigation.navigate("NetFalha",{erro: "exception erro network"});
        }
    }


    render(){
        return(
            <View style={estilo.container}>
                <Image style={estilo.imagemSuperior} source={require('../assets/imagemCadastro.png')}></Image>
                <View style={estilo.areaCadastro}>
                    <ScrollView>
                        <Text style={{color: '#5A6978',fontWeight: 'bold',marginTop: '5%',marginLeft:Dimensions.get("window").width/6,textTransform:'uppercase',fontSize:22}}>
                            <Icon name="arrow-left" size={30} style={{marginRight: Dimensions.get("window").width - 70, marginRight: 100}} onPress={ () => this.props.navigation.navigate("Login")} />
                            . INSCREVER-SE
                        </Text>
                        <TextInput value={this.state.user} onChangeText={ (text) => this.setState({user: text})} placeholder="Usuário" style={estilo.inputCadastro} inlineImageLeft="icon_cadastro" inlineImagePadding={20}></TextInput>
                        <TextInput value={this.state.nome} onChangeText={ (text) => this.setState({nome: text})} style={estilo.inputCadastro} placeholder="Nome" inlineImageLeft="icon_cadastro" inlineImagePadding={20}></TextInput>
                        <TextInput value={this.state.sobrenome} onChangeText={ (text) => this.setState({sobrenome: text})} style={estilo.inputCadastro} placeholder="Sobrenome" inlineImageLeft="icon_cadastro" inlineImagePadding={20}></TextInput>
                        <TextInput value={this.state.email} onChangeText={ (text) => this.setState({email: text})} style={estilo.inputCadastro} placeholder="E-mail" inlineImageLeft="icon_email" inlineImagePadding={20}></TextInput>
                        <TextInput value={this.state.senha} onChangeText={ (text) => this.setState({senha: text})} style={estilo.inputCadastro} secureTextEntry={true} placeholder="Senha" inlineImageLeft="senha" inlineImagePadding={20}></TextInput>
                        <Text style={{fontSize: 20, textAlign: "center"}}>{this.state.load}</Text>
                        <TouchableHighlight onPress={this._postData} style={estilo.btnCadastrar}>
                            <Text style={{textAlign: 'center',paddingTop: '3%',color: 'white',fontSize: 16}}>
                                CADASTRAR
                                .<Icon name="plus-circle" size={20}/>
                            </Text>
                        </TouchableHighlight>
                        <TouchableHighlight onPress={() => this.props.navigation.navigate("Login")} style={estilo.jaPossui}>
                            <Text style={{textAlign: 'center'}}>Já possui conta? Log in
                            </Text>
                        </TouchableHighlight>
                    </ScrollView>
                </View>
            </View>

        )
    }
}

const estilo = StyleSheet.create({
    imagemSuperior:{
        height: 259,
        width: Dimensions.get("window").width
    },
    container:{
        flex:1,
        height: Dimensions.get("window").height,
    },
    areaCadastro: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: '100%',
        backgroundColor: 'white'
    },
    inputCadastro:{
        borderBottomWidth: 2,
        borderBottomColor: 'gray',
        width: Dimensions.get("window").width - 70,
        fontSize: 16
    },
    btnCadastrar:{
        width: Dimensions.get("window").width - 70,
        height: 45,
        backgroundColor: "#B4EB82",
        marginTop: "4%",
        borderRadius: 3
    },
    jaPossui:{
        backgroundColor: "#BEEBFF",
        width: 200,
        borderRadius: 3,
        marginTop: '7%',
        marginLeft: Dimensions.get("window").width/7
    }
});