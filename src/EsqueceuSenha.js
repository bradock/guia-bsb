import React, {Component} from 'react'
import {Text,View,StyleSheet,Image,Dimensions,TextInput, TouchableHighlight,ScrollView,BackHandler} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class EsqueceuSenha extends Component{


    state = {
        email: "",
        log:   "Senha enviada via e-mail"
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }
    
    componentWillUnmount() {
        this.backHandler.remove()
    }
    
    handleBackPress = () => {
        this.props.navigation.navigate("Login"); // works best when the goBack is async
        return true;
    }

    // recuperar senha

    _getRecuperar = async () => {
        try{
            const email = this.state.email;
            return fetch('http://192.168.0.17:3000/recuperarSenha'+email)
            .then( response => response.json())
            .then(responseJson => {
                console.log(responseJson[0].recuperar)
                responseJson[0].recuperar >= 1 ? this.setState({log: "Senha enviada via e-mail" }) : this.setState({log : "Nenhum usuário cadastro com o e-mail informado"})
            })
        }catch(expection){
            console.log(expection);
            this.setState({log: ""});
            this.props.navigation.navigate("NetFalha",{erro: "exception erro network"});
        }
    }

    render(){
        return(
            <View style={estilo.container}>
                <Image style={estilo.imagemSuperior} source={require('../assets/imagemCadastro.png')}/>
                <View style={estilo.areaEsqueceu}>
                    <ScrollView >
                        <Icon name="arrow-left" size={30} style={{marginRight: Dimensions.get("window").width -120, marginBottom: 10,marginTop: 10}} onPress={ () => this.props.navigation.navigate("Login")} />
                        <TextInput value={this.state.email} onChangeText = {(text) => this.setState({email: text})} style={estilo.areaCampos} placeholder="Email" placeholderTextColor="#1F2D3D"/>
                        <Text style={{fontSize: 15, textAlign: "center",marginTop: 20}}>{this.state.log}</Text>
                        <TouchableHighlight onPress={this._getRecuperar} style={estilo.btnAtualizaSenha}>
                            <Text style={{textAlign: "center", color: "#fff", marginTop: 9, fontSize:16, textTransform: "uppercase"}}>Recuperar Senha</Text>
                        </TouchableHighlight>
                        <View style={estilo.lembrouSenha}>
                            <Text>Lembrou a senha? efetue o </Text>
                            <TouchableHighlight style={estilo.bntLogin} onPress={() => this.props.navigation.navigate("Login")}>
                                <Text style={{color: "#fff"}}> Login </Text>
                            </TouchableHighlight>
                            <Text> ou </Text>
                            <TouchableHighlight style={estilo.btnRegistrar} onPress={() => this.props.navigation.navigate("CadastroInicial")}>
                                <Text style={{color: "#fff"}}> Registre-se </Text>
                            </TouchableHighlight>
                        </View>
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const estilo = StyleSheet.create({
    container:{
        flex: 1,
        height: Dimensions.get("window").height,
    },
    imagemSuperior:{
        width: Dimensions.get("window").width,
        height: 259,
    },
    areaEsqueceu:{
        flex: 1,
        width: '100%',
        backgroundColor: "#fff",
        alignItems: 'center'
    },
    areaCampos: {
        borderBottomWidth: 2,
        borderBottomColor: "#C0CCDA",
        width: Dimensions.get("window").width - 70,
        fontSize: 16,
        marginTop: 2
    },
    btnAtualizaSenha: {
        marginTop: 30,
        width: Dimensions.get("window").width - 70,
        backgroundColor: "#B4EB82",
        height: 43
    },
    lembrouSenha:{
        flex: 1,
        flexDirection: "row",
        marginTop: 14,
        marginLeft: 0
    },
    bntLogin:{
        backgroundColor: "#BEEBFF",
        height: 22,
    },
    btnRegistrar:{
        backgroundColor: "#5A6978",
        height: 22,
    }
});