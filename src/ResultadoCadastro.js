import React, {Component} from 'react'
import {Text,View,Dimensions,StyleSheet,Image,TouchableHighlight,BackHandler} from 'react-native'

export default class ResultadoCadastro extends Component{

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
      }
    
      componentWillUnmount() {
          this.backHandler.remove()
      }
    
      handleBackPress = () => {
          this.props.navigation.navigate("Login"); // works best when the goBack is async
          return true;
      }

    render(){
        return(
            <View style={estilo.container}>
                <Image style={estilo.imagemSuperior} source={require('../assets/imagemCadastro.png')} />
                <View style={estilo.areaResultado}>
                    <Image style={estilo.imageSuccess} source={require("../assets/success.png")}/>
                    <Text style={estilo.txtResultado}>Cadastro com Sucesso.</Text>
                    <TouchableHighlight style={estilo.btnVoltar} onPress={() => this.props.navigation.navigate("Login")}>
                        <Text style={{color: "#fff",fontSize: 20,textAlign: "center",marginTop: 7}}>Voltar</Text>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

const estilo = StyleSheet.create({
    container:{
        flex: 1,
        height: Dimensions.get("window").height,
    },
    imagemSuperior:{
      width: Dimensions.get("window").width,
      height: 259  
    },
    areaResultado:{
        flex: 1,
        alignItems: "center",
        width: '100%',
        backgroundColor: "#fff"
    },
    txtResultado:{
        fontSize: 25,
        marginTop: 20,
        color: "#47525E"
    },
    btnVoltar:{
        backgroundColor: "#0998E7",
        width: Dimensions.get("window").width - 140,
        height: 43,
        marginTop: 30
    },
    imageSuccess:{
        marginTop: 25,
        width: 180,
        height: 180
    }
});