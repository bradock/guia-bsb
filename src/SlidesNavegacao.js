import React, {Component} from 'react'
import {Text,View, StyleSheet,Image,Dimensions,ScrollView} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import PropTypes from 'prop-types';


export default class SlidesNavegacao extends Component {

    render(){
        return(
            <View style={estilo.container}>
                 <View style={estilo.perfilMenu}>
                    <Image style={estilo.personImage} source={require("../assets/person.png")} />
                    <Text style={estilo.txtNome} onPress={() => this.props.navigation.navigate("Perfil")}>Francisco Hugo</Text>
                    <Text style={estilo.txtCategoria}>Viajante iniciante</Text>
                    
                 </View>
                <ScrollView>
                    <View style={estilo.itemMenu}>
                        <Text style={estilo.txtItemMenu} onPress={() => this.props.navigation.navigate("Mapa")}>
                            Mapa {'\u00A0'} {'\u00A0'} {'\u00A0'} {'\u00A0'} {'\u00A0'} {'\u00A0'} 
                        </Text>
                        <Icon name="home" size={25} color="#fff"/>
                    </View>
                    <View style={estilo.itemMenu}>
                        <Text style={estilo.txtItemMenu}>
                            Mensagem {'\u00A0'}
                        </Text>
                        <Icon name="envelope-open" size={25} color="#fff"/>
                    </View>
                    <View style={estilo.itemMenu}>
                        <Text style={estilo.txtItemMenu} onPress={() => this.props.navigation.toggleDrawer()}>
                            Rankings {'\u00A0'} {'\u00A0'} {'\u00A0'}
                        </Text>
                        <Icon name="gamepad" size={25} color="#fff"/>
                    </View>
                    <View style={estilo.itemMenu}>
                        <Text style={estilo.txtItemMenu}>
                            Câmera {'\u00A0'} {'\u00A0'} {'\u00A0'} {'\u00A0'}
                        </Text>
                        <Icon name="camera" size={25} color="#fff"/>
                    </View>
                    <View style={estilo.itemMenu}>
                        <Text style={estilo.txtItemMenu}>
                            Mochila {'\u00A0'} {'\u00A0'} {'\u00A0'} {'\u00A0'}
                        </Text>
                        <Icon name="shopping-bag" size={25} color="#fff"/>
                    </View>
                    <View style={estilo.itemMenu}>
                        <Text style={estilo.txtItemMenu} onPress={() => this.props.navigation.navigate("SairApp")}>
                            Sair {'\u00A0'} {'\u00A0'} {'\u00A0'} {'\u00A0'} {'\u00A0'} {'\u00A0'} {'\u00A0'}
                        </Text>
                        <Icon name="times" size={25} color="#fff"/>
                    </View>
                </ScrollView>
            </View>
        )
    }
}


const estilo = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: "#1E2D3E",
        width: '100%'
    },
    perfilMenu:{
        backgroundColor: "#263445",
        height: Dimensions.get("window").height/5,
        alignItems: "center",
        justifyContent: 'center'
    },
    personImage:{
        width: 70,
        height: 70
    },
    txtNome:{
        color: "#fff",
        fontSize: 25
    },
    txtCategoria:{
        color: "#fff",
        fontSize: 12
    },
    itemMenu:{
        flexDirection: "row",
        justifyContent: "center",
        marginTop: 45
    },
    txtItemMenu:{
        color: "#fff",
        fontSize: 18,
        marginRight: Dimensions.get("window").width/4,

    }

});