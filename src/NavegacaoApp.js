import React from 'react'
import {createDrawerNavigator,createStackNavigator, createAppContainer} from 'react-navigation'
import EsqueceuSenha from './EsqueceuSenha'
import CadastroInicial from './CadastroInicial'
import Login from './Login'
import PontoInteresse from './PontoInteresse'
import ResultadoCadastro from './ResultadoCadastro'
import Mapa from './Mapa'
import AtualizarCadastro from './AtualizarCadastro'
import Perfil from './Perfil'
import SlidesNavegacao from './SlidesNavegacao'
import SairApp from './SairApp'
import NetFalha from './NetFalha'

export default createAppContainer(
        createDrawerNavigator({
        Login,
        Perfil,
        Mapa,
        PontoInteresse,
        CadastroInicial,
        EsqueceuSenha,
        ResultadoCadastro,
        AtualizarCadastro,
        SairApp,
        NetFalha
        
    }, {
        drawerWidth: 300,
        contentComponent: SlidesNavegacao
        }
    )
);
