import React, {Component} from 'react'
import 
{Text,View, StyleSheet,Image,
TextInput,TouchableHighlight,Dimensions, KeyboardAvoidingView,BackHandler} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class Login extends Component{

    state = {
        email: "",
        senha: "",
        log: "",
        user: ""
        //user: this.props.navigation.getParam("user") != null ? : this.props.navigation.getParam("user")  
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }
    
    componentWillUnmount() {
        this.backHandler.remove()
    }
    
    handleBackPress = () => {
        this.props.navigation.navigate("Login"); // works best when the goBack is async
        return true;
    }

    teste2(){
        console.log(this.state.user);
    }

    _postLogin = async () =>{
        const email = this.state.email;
        const senha = this.state.senha;
        try{
            return fetch("http://192.168.0.17:3000/login/"+email+"/"+senha)
            .then(response => response.json())
            .then(responseJson => {
                //console.log(responseJson[0]);
                this.setState({
                    log: "",
                    email: "",
                    senha: "",
                    user: responseJson[0].user
                })
                responseJson[0].login >= 1 ? this.props.navigation.navigate("Perfil",{user: responseJson[0].user}) : this.setState({log: "Senha ou e-mail inválidos"}) 
            })
        }catch(expection){
            this.setState({
                log: "",
                email: "",
                senha: "",
            })
            console.log(exception);
            this.props.navigation.navigate("NetFalha",{erro: "exception erro network"});
        }
    }

    render(){
        return(
            <LinearGradient style={estilo.container}  colors={["#0093E9","#80D0C7"]}>
                <View>
                    <Image source={require('../assets/icon.png')} style={estilo.logo}/>
                </View>

                <View style={estilo.login}>
                    <TextInput value={this.state.email} onChangeText={(text) => this.setState({email: text})} style={estilo.designEmail} placeholder="Email" placeholderTextColor="white"
                        inlineImageLeft="person_login" inlineImagePadding={20}
                    >
                    </TextInput>
                
                    <TextInput value={this.state.senha} onChangeText={(text) => this.setState({senha: text})} style={estilo.designEmail} secureTextEntry={true} placeholder="Senha" placeholderTextColor="white"
                    inlineImageLeft="senha_login" inlineImagePadding={20}
                    >
                    </TextInput>
                    
                    <Text>{this.state.log}</Text>

                    <TouchableHighlight onPress={ () => this.props.navigation.navigate("EsqueceuSenha") }>
                        <Text style={estilo.esqueceuSenha }>Esqueceu a senha?</Text>
                    </TouchableHighlight>
                </View>

                <View style={estilo.areaLogar}>
                    <TouchableHighlight onPress={this._postLogin} style={estilo.btnLogin}>
                        <Text style={{textAlign:'center',marginTop: 10,color: 'white',fontSize: 16}}>LOGIN</Text>
                    </TouchableHighlight>
                    <TouchableHighlight onPress={() => this.props.navigation.navigate("CadastroInicial")} style={estilo.btnCadastrar}>
                        <Text style={{textAlign:'center',marginTop: 10,color: 'white',fontSize: 16}}>CADASTRAR</Text>
                    </TouchableHighlight>  
                </View>

                <View style={estilo.logarRedesSociais}>
                    <TouchableHighlight style={estilo.btnFacebook}>
                        <Text style={estilo.txtBtnRedesSociais} onPress={() => this.teste2()}>
                            <Icon name="facebook-square" size={20}/>
                            . Entrar com Facebook
                            </Text>
                    </TouchableHighlight>

                    <Text style={{padding: 7}}>OU</Text>

                    <TouchableHighlight style={estilo.btnGoogle}>
                        <Text style={estilo.txtBtnRedesSociais}>
                            <Icon name="google" size={20} color="#fff" />
                            . Entrar com o Google
                        </Text>
                    </TouchableHighlight>
                </View>
            </LinearGradient>
        )
    }
}
const estilo = StyleSheet.create({
    container:{
        flex: 1, // 1
        position: 'absolute',
        top: 0,
        bottom:0,
        left: 0,
        right: 0,
        alignItems: 'center',
        justifyContent: 'center',
        height: Dimensions.get("window").height,

    },
    designEmail:{
        borderBottomColor: 'white',
        borderBottomWidth: 2,
        fontSize: 18,
        width: Dimensions.get('window').width - 70,
        color: 'white',
        marginBottom: 20
    },  
    login:{
        //flex: 2, // 2
        //backgroundColor: "red",
        //width: '100%',
        marginTop: 5,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    areaLogar:{
        //flex: 2, // 2
        //width: '100%',
        //backgroundColor: 'blue',
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        marginTop: 10
    }, 
    logo:{
       width: 145,
       height: 145,
       marginTop: 25,
       marginBottom: 15
    },
    esqueceuSenha:{
        color: 'white',
        fontSize: 12,
    },
    btnLogin:{
        backgroundColor: '#BEEBFF',
        width: Dimensions.get('window').width - 70,
        height: 40,
        borderRadius: 2,
        marginTop: 10,
    },
    btnCadastrar:{
        backgroundColor: '#B4EB82',
        borderRadius: 2,
        height: 40,
        width: Dimensions.get('window').width - 70,
        marginTop: 20
    },
    logarRedesSociais:{
        flex: 2, // 2
        width: '100%',
        backgroundColor: '#FFFFFF',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
    },
    btnFacebook:{
        backgroundColor: '#3C5A99',
        height: 40,
        borderRadius: 2,
        width: Dimensions.get('window').width - 70
    },
    btnGoogle:{
        backgroundColor: '#D44638',
        height: 40,
        width: Dimensions.get("window").width - 70,
        borderRadius: 2,
        marginBottom: 20
    },
    txtBtnRedesSociais:{
        fontSize: 16,
        color: 'white',
        textTransform: 'uppercase',
        textAlign: 'center',
        paddingTop: '3%'
    },
});
