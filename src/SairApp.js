import React, {Component} from 'react'
import {Text,View,StyleSheet,Image,TouchableHighlight} from 'react-native'

export default class SairApp extends Component{
    render(){
        return(
            <View style={estilo.back}>
                <Image style={estilo.logo} source={require('../assets/icon.png')}/>
                <Text>GUIABSB</Text>
                <TouchableHighlight style={estilo.btnSair} onPress={() => this.props.navigation.navigate("Login",{user: null})}>
                    <Text style={{color:"white", fontSize:20, textAlign: "center"}}>Sair</Text>
                </TouchableHighlight>
            </View>
        )
    }
}

const estilo = StyleSheet.create({
    btnSair:{
        marginTop: 20,
        backgroundColor: "green",
        width: 70,
        height: 30
    },  
    back:{
        flex: 1,
        backgroundColor: "white",
        alignItems: "center",
        justifyContent: "center"
    },
    logo:{
        width: 120,
        height: 120,
    }
});