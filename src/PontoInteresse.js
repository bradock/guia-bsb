import React, {Component,PureComponent} from 'react'
import {Text,View,StyleSheet,TouchableHighlight,Image,Dimensions,ScrollView,BackHandler} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import LinearGradient from 'react-native-linear-gradient'

export default class PontoInteresse extends Component {

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }
    
    componentWillUnmount() {
        this.backHandler.remove()
    }
    
    handleBackPress = () => {
        this.props.navigation.navigate("Perfil"); // works best when the goBack is async
        return true;
    }


    state = {
        dataInaguracao: this.props.navigation.getParam("monumento").dta_inaguracao,
        idMonumento: this.props.navigation.getParam("monumento").pk_idt_monumento,
        nomeAutor: this.props.navigation.getParam("monumento").nme_autor,
        nomeMonumento: this.props.navigation.getParam("monumento").nme_monumento,
        txtInfo: this.props.navigation.getParam("monumento").txt_info,
        fkMacro: this.props.navigation.getParam("monumento").fk_idt_macro,
        foto: [
            require("../assets/img_monumento/torreTv"+".jpg"),
            require("../assets/img_monumento/meteoro"+".jpg"),
            require("../assets/img_monumento/palacioItamara"+".jpg"),
            require("../assets/img_monumento/marquise"+".jpg"),
            require("../assets/img_monumento/ostrower"+".jpg"),
            require("../assets/img_monumento/cassia"+".jpg"),
            require("../assets/img_monumento/plinio"+".jpg"), 
            require("../assets/img_monumento/feiraTorre"+".jpg"),
            require("../assets/img_monumento/torreTv"+".jpg"), 
            require("../assets/img_monumento/amoBsb"+".jpg"),
            require("../assets/img_monumento/teatroNacional"+".jpg"),
            require("../assets/img_monumento/bibliotecaNacional"+".jpg"),
            require("../assets/img_monumento/museuHonestino"+".jpg"),
            require("../assets/img_monumento/catedral"+".jpg"),
            require("../assets/img_monumento/estatuaEvan"+".jpg"),
            require("../assets/img_monumento/campanario"+".jpg"),
            require("../assets/img_monumento/jardimBurle"+".jpg"),
            require("../assets/img_monumento/escadaHelicoidal"+".jpg"),
            require("../assets/img_monumento/palacioJusti"+".jpg"),
            require("../assets/img_monumento/congressoNacional"+".jpg"),
            require("../assets/img_monumento/lucioCosta"+".jpg"),
            require("../assets/img_monumento/casaCha"+".jpg"),
            require("../assets/img_monumento/museuCidade"+".jpg"),
            require("../assets/img_monumento/pombal"+".jpg"),
            require("../assets/img_monumento/doisCandangos"+".jpg"),
            require("../assets/img_monumento/panteao"+".jpg"),
            require("../assets/img_monumento/bandeira"+".jpg"),
            require("../assets/img_monumento/chamaEterna"+".jpg"),
            require("../assets/img_monumento/palacioSupremo"+".jpg"), 
            require("../assets/img_monumento/estatuaJusti"+".jpg"),
            require("../assets/img_monumento/capelaFatima"+".jpg"),
            require("../assets/img_monumento/escolaParque"+".jpg"),
            require("../assets/img_monumento/escolaClasse"+".jpg"),
            require("../assets/img_monumento/jardimBurleMarx"+".jpg"),
            require("../assets/img_monumento/clubeVizinha"+".jpg"),
            require("../assets/img_monumento/renatoRusso"+".jpg"),
            require("../assets/img_monumento/jardimInfancia"+".jpg"),
            require("../assets/img_monumento/jk"+".jpg"),
            require("../assets/img_monumento/EstatuaJk"+".jpg"),
            require("../assets/img_monumento/memorialIndigena"+".jpg"),
            require("../assets/img_monumento/palacioBuriti"+".jpg"),
            require("../assets/img_monumento/pracaBuriti"+".jpg"),
            require("../assets/img_monumento/ulusses"+".jpg"),
            require("../assets/img_monumento/planetario"+".jpg"), 
            require("../assets/img_monumento/rodo"+".jpg"),
            require("../assets/img_monumento/esfera"+".jpg"),
            require("../assets/img_monumento/loba"+".jpg"),
            require("../assets/img_monumento/cruzeiro"+".jpg"), 
            require("../assets/img_monumento/marcoPatrimonio"+".jpg"),
        ]
    }

   


    render(){
        return(
            <ScrollView style={{flex: 1}}>
                <View style={estilo.container}>
                    <View style={estilo.barraSuperior}>
                    <TouchableHighlight onPress={() => this.props.navigation.navigate("Perfil")}>
                        <Icon style={{marginLeft: 15}} name="arrow-left" size={25} color="#fff"/>
                    </TouchableHighlight>
                        <TouchableHighlight>
                            <Text onPress={() => console.log(this.props.navigation.getParam("monumento"))} style={estilo.txtSuperior}>
                                {this.state.nomeMonumento}
                            </Text>
                        </TouchableHighlight> 
                    </View>


        
                    <Image style={estilo.imageLocal} source={this.state.foto[this.state.idMonumento-1]}/>
                    <LinearGradient style={estilo.gradiente} colors={["#2C89E6","#28B0D0"]}>
                        <Text style={estilo.txtInfo}>Dentro do Conj. Torre de TV </Text>
                        <Text style={estilo.txtInfo}>Circuito Fontes e Espelho D'água </Text>
                    </LinearGradient>

                    <View style={{display: "flex", flexDirection: "row"}}>
                        <View >
                            <Text style={{color: "#1F2D3D", fontSize: 20, marginLeft: 10}}>
                                Monumento
                            </Text>
                        
                            <Text style={estilo.txtDescricao}>
                                <Text style={{fontWeight: "bold"}}>Autor:</Text> {this.state.nomeAutor}
                            </Text>
                            <Text style={estilo.txtDescricao}>
                                Inaugurada em {this.state.dataInaguracao}
                            </Text> 
                        </View>
                        
                        <View style={{flexDirection:"row",alignItems: "flex-start", marginLeft: Dimensions.get("window").width/16}}>
                            <Text style={{fontSize: 15}}>Possui:</Text>
                            <Icon style={estilo.icones} name="shower" size={30} color="#2C89E6"/> 
                            <Icon style={estilo.icones} name="shopping-basket" size={30} color="#2C89E6"/>
                            <Icon style={estilo.icones} name="users" size={30} color="#2C89E6"/>    
                        </View>
                       
                    </View>

                    <Text style={estilo.txtDescriacaoNota}>
                        <Icon name="history" size={15} />
                        . {this.state.txtInfo}
                    </Text>
                    <Text style={estilo.txtDescriacaoNota}>
                    <Icon name="info-circle" size={15} />
                        . 
                    </Text>
                </View>
             </ScrollView>
        )
    }
}


const estilo = StyleSheet.create({
    txtInfo :{
        color: "white",
        fontSize: 16,
        textAlign: "center",
        marginTop: 3
    },  
    gradiente:{
        height: 60,
        marginBottom: 20
    },
    icones:{
        textAlign: "center",
        marginLeft: 10,
    },  
    container:{
        flex: 1,
        width: '100%',
        height: Dimensions.get("window").height,
        backgroundColor: '#EDF0F6'
    },
    barraSuperior:{
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: "green",
        height: 50,
        backgroundColor: "#B4EB82"
    },
    txtSuperior:{
        fontSize: 25,
        marginLeft: 40,
        color: "#fff"
    },
    imageLocal:{
        width: Dimensions.get("window").width,
        height: '40%'
    },
    txtDescricao:{
        fontSize: 16,
        marginLeft: 10
    },
    txtDescriacaoNota:{
        fontSize: 17,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        textAlign: 'justify',
        borderBottomWidth: 1,
        borderBottomColor: "#8492A6",
    }
});