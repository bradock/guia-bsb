import React from 'react'
import {Text,View,StyleSheet,Image} from 'react-native'

export default props => 
    <View style={estilo.container}>
        <View style={estilo.redondo}>
            <Image style={estilo.circulo} source={require("../assets/person.png")}/>
        </View>
        <Text style={estilo.back}>{props.nome}</Text>
        <View style={{flexDirection: "row"}}>
            <View style={estilo.circulin}>
                    <Text style={{color: "white", textAlign: "center",paddingTop: 2,fontWeight: "bold"}}>LVL 1</Text>
            </View>
            <View style={estilo.circulin}>
                    <Text style={{color: "white", textAlign: "center",paddingTop: 2, fontWeight: "bold"}}>00/2000</Text>
            </View>
        </View>
    </View>

const estilo = StyleSheet.create({
    circulin:{
        backgroundColor: "#2B8EE3",
        color: "white",
        borderRadius: 20,
        textAlign: "center",
        marginTop: 2,
        paddingTop: 2,
        fontWeight: "bold",
        fontSize: 14,
        width: 60,
        height: 30,
    },
    redondo:{
        borderRadius: 60,
        borderWidth: 6,
        borderColor: "#2B8EE3",
        width: 105,
        height: 108,
    },
    circulo:{
        width: 95,
        height: 95,
    },
    container:{
        flex:1,
        marginTop: 50,
    },
    back: {
        backgroundColor: "#2B8EE3",
        color: "white",
        borderRadius: 20,
        textAlign: "center",
        marginTop: 2,
        paddingTop: 2,
        fontWeight: "bold",
        fontSize: 14,
        width: 120,
        height: 30,
        marginRight: 10
    }
})