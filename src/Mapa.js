import React, {Component} from 'react'
import {Text,View,StyleSheet,Image,Dimensions,BackHandler} from 'react-native'
import MapView,{Polygon,Circle,Marker} from 'react-native-maps'
import Hud from './Hud'
export default class App extends Component{


  componentDidMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
   // this._getMacro()
  }

  componentWillUnmount() {
      this.backHandler.remove()
  }

  handleBackPress = () => {
      this.props.navigation.navigate("Perfil"); // works best when the goBack is async
      return true;
  }

  componentWillMount(){ // 1 Vez antes do render
    this._getMacro()
    this._getMonumentos()
  }

  _getMacro = async () => {
      try{
        return fetch("http://192.168.0.17:3000/macros")
        .then(response => response.json())
        .then(responseJson =>{
            //console.log(responseJson[0])
            var arrayMacroLat  = [];
            var arrayMacroLog  = [];
            var arrayMacroRaio = [];
            responseJson.forEach(function(valor, indice){
              //console.log(valor);
              arrayMacroLat.push(valor.lat_macro)
              arrayMacroLog.push(valor.lgt_macro)
              arrayMacroRaio.push(valor.raio)
            });
            this.setState({
              circulosMacroLat: arrayMacroLat,
              circulosMacroLog: arrayMacroLog,
              circulosMacroRaio: arrayMacroRaio
            })
        })
    }catch(exception){
        console.log(exception);
        this.props.navigation.navigate("NetFalha",{erro: "exception erro network"});
    }
  }

  _getMonumentos = async () => {
    try{
      return fetch("http://192.168.0.17:3000/monumentos")
      .then(response => response.json())
      .then(responseJson =>{
          var lat     = [];
          var long    = [];
          var titulo  = [];
          var monumet = [];
          responseJson.forEach(function(valor, indice){
            //console.log(valor)
            lat.push(valor.lat_monumento)
            long.push(valor.lgt_monumento)
            titulo.push(valor.nme_monumento)
            monumet.push(valor)
          });
          this.setState({
            monumentoLatitude: lat,
            monumentoLongitude: long,
            monumentoTitulo: titulo,
            monumento: monumet
          })
      })
  }catch(exception){
      console.log(exception);
      this.props.navigation.navigate("NetFalha",{erro: "exception erro network"});
  }
}
  
  state = {
    latitude: -15.799777,
    longitude: -47.864195,
    poly: [
      {latitude: -15.794573,
      longitude: -47.883816,},
      {latitude: -15.791393,
      longitude: -47.896031,},
      {latitude: -15.788193,
      longitude: -47.894926,},
      {latitude: -15.792539,
      longitude: -47.883146,}
    ],
    circulosMacroLat:  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    circulosMacroLog:  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    circulosMacroRaio: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    coresMacro: [
      "rgba(106,90,205,0.5)",
      "rgba(25,25,112,0.5)",
      "rgba(100,149,237,0.5)",
      "rgba(70,130,180,0.5)",
      "rgba(32,178,170,0.5)",
      "rgba(47,79,79,0.5)",
      "rgba(46,139,87,0.5)",
      "rgba(128,128,0,0.5)",
      "rgba(188,143,143,0.5)",
      "rgba(75,0,130,0.5)",
      "rgba(218,112,214,0.5)",
      "rgba(220,20,60,0.5)",
      "rgba(255,69,0,0.5)",
      "rgba(240,230,140,0.5)",
      "rgba(238,232,170,0.5)",
      "rgba(240,255,240,0.5)",
      "rgba(216,191,216,0.5)"
    ],
    monumento: [
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    ],
    monumentoLatitude: [
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    ],
    monumentoLongitude: [
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    ],
    monumentoTitulo: [
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    ]
  }

  render(){
     return(
       <View style={estilo.container}>
          <MapView style={estilo.mapa}
            initialRegion={{
                latitude: -15.799777,
                longitude: -47.864195,
                latitudeDelta: 0.0450,
                longitudeDelta: 0.0450,
            }}
            showPointsOfInterest={false}
            showsBuildings={false}
            >

          <View >
              {this.state.circulosMacroRaio.map((prop, key) => {
                return (
                  <MapView.Circle
                    center={{latitude: this.state.circulosMacroLat[key],
                      longitude: this.state.circulosMacroLog[key]}}
                      radius={this.state.circulosMacroRaio[key]}
                      fillColor={this.state.coresMacro[key]}
                      zIndex={1}
                  />
                )
              })}
          </View>
          <View>
              {this.state.monumento.map((prop, key) =>{
                return(
                  <MapView.Marker 
                  coordinate={{latitude: this.state.monumentoLatitude[key], longitude: this.state.monumentoLongitude[key]}}
                  title={this.state.monumentoTitulo[key]+""}
                  onPress={() => this.props.navigation.navigate("PontoInteresse",{monumento: this.state.monumento[key]})}
                  pinColor="yellow"
                  opacity= {0.8}
                  />
                )
              })}

          </View>


          <MapView.Polygon
              coordinates={this.state.poly}
              fillColor="rgba(72,61,139,0.5)"
          />
          <MapView.Marker 
              onPress={() => this.props.navigation.navigate("PontoInteresse")}
              coordinate={{latitude: this.state.latitude, longitude: this.state.longitude}}
              title="Referente ao SS do Barbosa"
              pinColor="yellow"
              opacity= {0.8}
          />

          <MapView.Marker 
              coordinate={{latitude: this.state.monumentoLatitude[1], longitude: this.state.monumentoLongitude[1]}}
              title={this.state.monumentoTitulo[1]+""}
              pinColor="yellow"
              opacity= {0.8}
          />

          </MapView>
          <Hud nome="Francisco Hugo"/>
          <View style={estilo.espacoIcone}>
                <Text onPress={() =>  this.props.navigation.toggleDrawer()}>oi</Text>
                <Text onPress={() => console.log(this.state.monumentoLongitude)}>oi</Text>
                <Text onPress={() => console.log(this.state.monumentoTitulo)}>oi</Text>
                <Text onPress={() => console.log(this.state.monumento)}>oi</Text>
                <Image style={estilo.imgIcones} source={require("../assets/mapa_icone.png")}/>
          </View>
          <View style={estilo.espacoIconeBaixoMsg}>
                <Image style={estilo.imgIcones} source={require("../assets/msg_icone.png")} />
                <Image style={estilo.imgIcones} source={require("../assets/icon-ranking.png")} />
                <Image style={estilo.imgIcones} source={require("../assets/icon_foto.png")} />
                <Image style={estilo.imgIcones} source={require("../assets/icon_mochila.png")} />
          </View>
       </View>
     ) 
  }
}

const estilo = StyleSheet.create({
  container:{
    flex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  mapa:{
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  imgIcones:{
    borderRadius: 90,
    borderColor: "#343F4B",
    width: 60,
    height: 60,
    borderWidth: 4,
    marginLeft: 30
  },
  espacoIcone:{
    marginLeft: Dimensions.get("window").width - 130,
    marginBottom: 40
  },
  espacoIconeBaixoMsg:{
    marginLeft: Dimensions.get("window").width - 400,
    marginBottom: 50,
    flexDirection: "row",
    justifyContent: 'center'
  },
});
