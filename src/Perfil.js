import React, {Component} from 'react'
import {Text,View,StyleSheet,Image,Dimensions,ScrollView,Button,TouchableHighlight,BackHandler} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class Perfil extends Component{

    state = {
        user: this.props.navigation.getParam("user"),
        nome: "",
        titulo: "",
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this._getPerfil()
    }

    teste(){
        console.log(this.state.user);
    }
    
    componentWillUnmount() {
        this.backHandler.remove()
        this.setState({
            nome: "",
            titulo: ""
        })
    }

    
    handleBackPress = () => {
        this.props.navigation.navigate("Perfil",{user: this.state.user}); // works best when the goBack is async
        return true;
    }

    _getPerfil = async () =>{
        try{
            const user = this.state.user;
            return fetch("http://192.168.0.17:3000/perfil/"+user)
            .then(response => response.json())
            .then(responseJson =>{
                console.log(responseJson[0])
                this.setState({
                    nome: responseJson[0].nme_usuario,
                    titulo: responseJson[0].txt_titulo,
                })
            })
        }catch(exception){
            console.log(exception);
            this.props.navigation.navigate("NetFalha",{erro: "exception erro network"});
        }
    }

    render(){
        return(
            <View style={estilo.container}>
                <Image style={estilo.imagemSuperior} source={require("../assets/imagemAtualizarCadastro.png")} />
                <Image style={estilo.personFoto} source={require("../assets/person.png")}/>
                <TouchableHighlight onPress={() => this.props.navigation.toggleDrawer()}>
                    <Icon name="align-left" size={30} style={{marginLeft: 20}}   />
                </TouchableHighlight>
                <View style={estilo.areaDados}>
                    <Text style={{fontSize: 25, color:"#47525E",textAlign: "center",marginBottom: 5}} onPress={() => this.props.navigation.navigate("AtualizarCadastro",{user: this.state.user}) }>{this.state.nome} .
                         <Icon name="pencil" size={30}/>
                    </Text>
                    <Text style={estilo.txtInfo} onPress={() => this.teste()}>
                        {this.state.titulo}
                    </Text>  
                    <Text onPress={() => this._getPerfil()} style={estilo.txtInfo}>
                        Atualmente em Brasília, DF
                    </Text>
                </View>
                <ScrollView horizontal={true}>
                    <View style={estilo.painelHistorico}>
                        <Text style={estilo.txtInfoHistorico}>
                            Novo viajante
                        </Text>
                        <Text style={{marginTop: 5, fontSize: 12, textAlign: 'center',color: 'black'}}>
                            Acabou de entrar na aplicação
                        </Text>
                        <Image style={{marginLeft: 60, marginTop: 5, width: 70, height: 70}} source={require("../assets/trofeu.png")} />
                        <Text style={{color: "black", fontSize: 20,marginLeft: 60}}>
                            +20 pts
                        </Text>
                    </View>
                    <View style={estilo.painelHistorico}>
                        <Text style={estilo.txtInfoHistorico}>
                            Visitou a Torre de TV
                        </Text>
                        <Text style={{marginTop: 5, fontSize: 12, textAlign: 'center',color: 'black'}}>
                            Entrou no raio da Torre de TV
                        </Text>
                        <Image style={{marginLeft: 60, marginTop: 5, width: 70, height: 70}} source={require("../assets/trofeu.png")} />
                        <Text style={{color: "black", fontSize: 20,marginLeft: 60}}>
                            +70 pts
                        </Text>
                    </View>
                    <View style={estilo.painelHistorico}>
                        <Text style={estilo.txtInfoHistorico}>
                            Região Macro
                        </Text>
                        <Text style={{marginTop: 5, fontSize: 12, textAlign: 'center',color: 'black'}}>
                            Finalizou todas as regiões macros
                        </Text>
                        <Image style={{marginLeft: 60, marginTop: 5, width: 70, height: 70}} source={require("../assets/trofeu.png")} />
                        <Text style={{color: "black", fontSize: 20,marginLeft: 60}}>
                            +1000 pts
                        </Text>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const estilo = StyleSheet.create({
    options:{
        width: 40,
        height:40,
        marginTop: -40,
        marginLeft: 20
    },
    container:{
        flex: 1,
        width: '100%',
        backgroundColor: "white"
    },
    imagemSuperior:{
        height: 259,
        width: Dimensions.get("window").width
    },
    personFoto:{
        width: 140,
        height: 140,
        marginTop: -90,
        marginLeft: Dimensions.get("window").width /3
    },
    areaDados : {
        marginTop: 5,
        alignItems: 'center',
        marginBottom: 15
    },
    txtInfo:{
        fontSize: 16
    },
    painelHistorico: {
       width: Dimensions.get("window").width/2,
       backgroundColor: "#C0CCDA",
       height: Dimensions.get("window").height/4,
       marginRight: 40 
    },
    txtInfoHistorico:{
        backgroundColor: "#EFF2F7",
        fontSize: 19,
        textAlign: "center",
        color: "#47525E"
    }
});