import React, {Component} from 'react'
import {Text,View,StyleSheet} from 'react-native'
import MapView from 'react-native-maps'
import Login from './src/Login'

export default class App extends Component{
  render(){
     return(
       <View style={estilo.container}>
        <MapView style={estilo.mapa}
        initialRegion={{
            latitude: -15.799777,
            longitude: -47.864195,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        }}>
       </MapView>
       <Login></Login>
       </View>
     ) 
  }
}

const estilo = StyleSheet.create({
  container:{
    flex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  mapa:{
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  }
});